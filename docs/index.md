# Welcome to ArcadeMVS/wiki

![avatar](about/avatar.png)

ArcadeMVS is a GitLab group of repositories related to creating a custom Multi-Video-System for home use.

---

## Discord

[Discord](https://discordapp.com) is a multi-platform chat server. You may message the group owner with wiki contributions. It's also suggested to join servers to chat with others to find the help you need.

Users:

* Group Owner: ```keilmillerjr#1590```

Servers:

* [AttractMode](https://discord.gg/86bB9dD)
* [ArcadeMVS](https://discord.gg/tJmWCgf)

---

## Git

This wiki is a git repository (code versioning and collaboration), hosted on GitLab. It uses [MkDocs](https://www.mkdocs.org), a wiki site generator. It comes in the form of a pip package. [pip](https://pip.readthedocs.io/en/stable/installing/) is a [python](https://www.python.org) package manager. The Git repository only needs to contain the markdown files. It's static pages are built by [GitLab's Continuous Integration](https://about.gitlab.com/gitlab-ci/).

1. Install Git, if not installed ```$ git --version```
    * Debian: ```$ sudo apt install git-all```
    * Mac [Homebrew](https://brew.sh): ```$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"``` to install homebrew and then ```$ brew install git``` to install Git.
    * Mac Command Line Tools (Mac 10.9+): ```$ xcode-select --install```
    * Mac Installer: Download and run installer from [https://git-scm.com/download/mac](https://git-scm.com/download/mac).
    * Windows: Download and run installer from [https://git-scm.com/download/win](https://git-scm.com/download/win).
2. Fork the wiki repository.
    1. Navigate your browser to [https://gitlab.com/arcademvs/wiki](https://gitlab.com/arcademvs/wiki).
    2. Click the fork icon.
    3. Choose a namespace to fork your project to.
3. Clone the repo with SSH or HTTPS. Click the clone icon for your link.
    * ```$ git clone git@gitlab.com:namespace/wiki.git```
    * ```$ git clone https://gitlab.com/namespace/wiki.git```
4. More steps to come. This section is UNFINISHED.

---

## Donate

It takes a lot of time to document this wiki, patch and compile MAME, write and support AttractMode layouts/modules/plugins. Any donations would help me to maintain my hardware used for such open source projects.

To donate using PayPal, click the [Donate button](#donate) at the bottom of the page, or click/scan the following:

[![PayPal QR-Code](about/paypal-qr.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=keilmillerjr%40me.com&item_name=ArcadeMVS&currency_code=USD&source=url)

---

## Projects

Below is an overview of the ArcadeMVS subgroups and projects. To view the projects, visit the [ArcadeMVS](https://gitlab.com/arcademvs) namespace on GitLab. Each project should have a readme.

```
ArcadeMVS
├── attract
│   ├── plugins
│   │   ├── debug
│   │   ├── fadetogame
│   │   ├── leap
│   │   └── sequencer
│   ├── modules
│   │   ├── debug
│   │   ├── helpers
│   │   ├── shader
│   │   └── shuffle
│   └── layouts
│       ├── flavors
│       ├── mvscomplete
│       ├── picknmix
│       └── playchoicecomplete
├── mame
└── wiki
```
