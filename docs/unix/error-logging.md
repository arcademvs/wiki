# Error Logging

Logging errors is important for sharing an issue with others. Many developers will not even listen to an issue without a log file.

---

## Terminal Syntax

When passing a command to the terminal, you can append the following syntax and a filename.

Syntax | Terminal STDOUT | Terminal STDERR | File STDOUT | File STDERR | File Mode
:---: | :---: | :---: | :---: | :---: | :---:
`>` | no | yes | yes | no | overwrite
`>>` | no | yes | yes | no | append
|||||
`2>` | yes | no | no | yes | overwrite
`2>>` | yes | no | no | yes | append
|||||
`&>` | no | no | yes | yes | overwrite
`&>>` | no | no | yes | yes | append
|||||
`| tee` | yes | yes | yes | no | overwrite
`| tee -a` | yes | yes | yes | no | append
|||||
does not exist |   yes    |   yes    |    no    |   yes    | overwrite
does not exist |   yes    |   yes    |    no    |   yes    |  append
|||||
`|& tee` | yes | yes | yes | yes | overwrite
`|& tee -a` | yes | yes | yes | yes | append

## Usage Examples

#### Attract Mode

Command | Description
:--- | :---
`./attract --config config |& tee log.txt` | Start attract binary with config folder path. Output STDERR and STDOUT to terminal (default), overwrite STDERR and STDOUT to `log.txt`.
`./attract --config config 2> error.txt` | Start attract binary with config folder path. Output STDOUT to terminal, overwrite STDERR to `error.txt`.

#### MAME

Command | Description
:--- | :---
`./mame64 -v alexkidd |& tee log.txt` | Start mame64 binary with internal diagnostic information (-v) and run Alex The Kidd rom. Output STDERR and STDOUT to terminal (default), overwrite STDERR and STDOUT to `log.txt`.
`./mame64 --config config 2> error.txt` | Start mame64 binary with internal diagnostic information (-v) and run Alex The Kidd rom. Output STDOUT to terminal, overwrite STDERR to `error.txt`.
