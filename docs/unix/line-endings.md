# Line Endings

In DOS/Windows, a line break is Carriage Return + Line Feed *\r\n*. In Unix (Linux, Mac OS X), a line break is Line Feed *\n*. In Classic Mac OS, Commodore and TRS-80, a line break was CR *\r*. I encountered errors while compiling MAME due to uniformity.

[dos2unix](https://waterlan.home.xs4all.nl/dos2unix.html) is a common utility, but I like to use [endlines](https://github.com/mdolidon/endlines). It's very simple and supports checking and recursive.

---

## endlines

#### Install endlines

*endlines* may be compiled on Linux, Mac OS X, and IBM AIX.

```
$ cd ~/
$ curl -Lo endlines192.zip "https://github.com/mdolidon/endlines/archive/1.9.2.zip" && unzip endlines192.zip && mv endlines-master endlines192
$ cd endlines192
$ make
$ make test
$ sudo make install
```

#### Convert Line Endings

**WARNING**: You must `cd` (change directory) to your target directory before issuing a command to convert files. There is no confirmation. In the following example, we will use the `mame0215` directory.

```
$ cd ~/mame0215
$ endlines check * -r # Will check all files in directory
$ endlines unix * -r # Convert all files in directory to unix format, recursively
```
