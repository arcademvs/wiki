# Compile MAME For Unix (Mac OS X and Linux)

Compiling MAME for Mac OS X and Linux is very similar because they both are based on Unix. However, dependancies are different. Take note of them throughout the guide.

[Official Compiling MAME Docs](https://docs.mamedev.org/initialsetup/compilingmame.html)

We will be using MAME version 0.213 as an example. You may need to substitute where necessary depending on which version you would like to compile.

---

## Arch Linux

#### IgnorePkg

If running patched linux kernel for 15khz (GroovyArcade), you must tell pacman to not update the kernel.

```
$ sudo nano /etc/pacman.conf
```

Find `#IgnorePkg   =` and change to `IgnorePkg   = linux`

#### Install Dependancies

[7-Zip](https://www.7-zip.org) is not required to compile MAME. However, it is recommended to pack/unpack binaries. It can compress files to a smaller size than zip.

unzip is required to decompress the MAME source that we download.

```
$ sudo pacman -Syu # Update all packages on the system.
$ sudo pacman -S unzip p7zip # Install dependancies
$ sudo pacman -S base-devel git sdl2 gconf sdl2_ttf gcc qt5 # Install dependancies for compiling MAME
```

---

## Mac OS X

#### Install Command Line Developer Tools

Mac OS X 10.9+ can use the following terminal command to install command line developer tools. Mac OS X 10.7.3 can download from [here](https://developer.apple.com/download/more/).

```
$ xcode-select --install # Install command line developer tools
```

#### Install SDL2 via Disk Image

Download disk image 2.0.4+ [here](https://www.libsdl.org/download-2.0.php) and then copy the framework to appropriate directory. It is recommended to install this way instead of using Homebrew. The Homebrew SDL2 formulae does not create a Mac style framework. See [here](https://wiki.libsdl.org/FAQMacOSX) for more details.

```
$ hdiutil attach /path/to/SDL2-2.0.10.dmg
$ sudo cp -R /Volumes/SDL2/SDL2.framework /Library/Frameworks/SDL2.framework
$ hdiutil detach /Volumes/SDL2
$ rm /path/to/SDL2-2.0.10.dmg
```

#### 7-Zip

[7-Zip](https://www.7-zip.org) is not required to compile MAME. However, it is recommended to pack/unpack binaries. It can compress files to a smaller size than zip. [Homebrew](https://brew.sh) is the missing package manager for macOS (or Linux).

```
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" # Install Homebrew if haven't already
$ brew update # Update Homebrew
$ brew install p7zip # Install 7-Zip
```

---

## MAME Source

Download and unzip MAME source.

```
$ cd ~/
$ curl -Lo mame0215s.zip https://github.com/mamedev/mame/releases/download/mame0215/mame0215s.zip && unzip mame0215s.zip && unzip mame.zip -d mame0215
$ rm mame.zip
```

---

## Line Endings

You must convert the line endings of MAME source files to avoid errors. Please read [Line Endings](line-endings.md). Instruction only will be provided in this section.

#### Install endlines

*endlines* may be compiled on Linux, Mac OS X, and IBM AIX.

```
$ cd ~/
$ curl -Lo endlines192.zip "https://github.com/mdolidon/endlines/archive/1.9.2.zip" && unzip endlines192.zip && mv endlines-master endlines192
$ cd endlines192
$ make
$ make test
$ sudo make install
```

#### Convert Line Endings

**WARNING**: You must `cd` (change directory) to your target directory before issuing a command to convert files. There is no confirmation. In the following example, we will use the `mame0215` directory.

```
$ cd ~/mame0215
$ endlines check * -r # Will check all files in directory
$ endlines unix * -r # Convert all files in directory to unix format, recursively
```

---

## Patches

Patches are optional, and can be found at [arcademvs/mame](https://gitlab.com/arcademvs/mame). The patches you apply may not be the same as shown below. I suggest reading the readme in the repo before continuing.

#### Download

```
$ cd ~/mame0215
$ curl -Lo mame0215_groovymame_017o.diff https://gitlab.com/arcademvs/mame/raw/master/diffs/mame0215_groovymame_017o.diff
$ curl -Lo mame0215_suppression.diff https://gitlab.com/arcademvs/mame/raw/master/diffs/mame0215_suppression.diff
$ curl -Lo mame0215_various_fixes.diff https://gitlab.com/arcademvs/mame/raw/master/diffs/mame0215_various_fixes.diff
```

#### Apply Patches

We use the *patch* utility. We are going to use the following options:

Options | Description
:--- | :---
`-p0` | Gives the entire file name unmodified.
`-E` or `--remove-empty-files` | Remove  output  files  that  are  empty  after the patches have been applied.

```
$ cd ~/mame0215
$ patch -p0 -E < mame0215_groovymame_017o.diff
$ patch -p0 -E < mame0215_suppression.diff
$ patch -p0 -E < mame0215_various_fixes.diff
```

---

## Compile

We use the *make* utility. We are going to use the following options:

Options | Description
:--- | :---
`-j` | Optional and proceeding the flag with an integer determines the amount of parallel jobs (to speed up compiling). Integer should match the amount of cpu cores. The variables used in the example will automatically determine how many cores your processor has.
`NOWERROR=1` | Disables treating compiler warnings as errors.

#### Linux

```
$ cd ~/mame0213
$ make -j$(nproc) NOWERROR=1
```

#### Mac OS X

```
$ cd ~/mame0213
$ make -j$(sysctl -n hw.ncpu) NOWERROR=1
```
