# Terminology

Some of the terminology commonly used in unix systems can be confusing.

---

## Computer/System

#### Computer/System Terminal (TTY)

The *terminal* is an electronic or electromechanical device used for communicating data with a computer or computing system. This once included teletypewriters (TTY), punched card readers and printers.

We now have computers with a keyboard and video monitor. The *linux terminal* is a piece of software that sends your input and receives output. It does not process your input.

#### Computer/System Console

A computer *console* is the output text entry and display device for system messages via a terminal. This includes a keyboard and video monitor.

A *linux console* is a way for the kernel and other processes to output text-based messages to the user as well as receive text-based input. The console will multiplex multiple virtual terminals.

#### Unix Shell (Command Line Interpreter)

A *unix shell* is a piece of software that users employ to type commands, utilizing command languages and scripting languages. This includes sh, ksh,	csh, tcsh, bash and zsh.
