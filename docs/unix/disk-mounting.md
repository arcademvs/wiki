# Disk Mounting

If your in the console, or running a desktop environment (DE) without auto-mounting, you may have to mount disks yourself. This is useful to transfer files from an external disk to your PC with GroovyArcade.

---

## Mount

[USB Storage Devices](https://wiki.archlinux.org/index.php/USB_storage_devices)

Run `$ sudo fdisk -l` to show logical disks. An example of output from this command would be the following:

```
Disk /dev/sda: 32 GiB, 34359738368 bytes, 67108864 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xe3e7c29b

Device     Boot   Start      End  Sectors  Size Id Type
/dev/sda1  *       2048   821247   819200  400M 83 Linux
/dev/sda2        821248  4917247  4096000    2G 82 Linux swap / Solaris
/dev/sda3       4917248 67108863 62191616 29.7G 83 Linux


Disk /dev/sdb: 3.65 TiB, 4000752599040 bytes, 7813969920 sectors
Disk model: easystore 25FA  
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: F76DD893-182F-4342-9A4D-5DD7EC5D8F57

Device     Start        End    Sectors  Size Type
/dev/sdb1   2048 7813967871 7813965824  3.7T Microsoft basic data
```

Create a directory to mount disk, and then mount the disk to that location. We will mount my USB hard drive shown in the previous example.

```
sudo mkdir /mnt/usbdisk/
sudo mount /dev/sdb1 /mnt/usbdisk/
```

You may now access your data from `/mnt/usbdisk/`.

---

## Unmount

[Unmount a File System](https://wiki.archlinux.org/index.php/File_systems#Unmount_a_file_system)

You may use `sudo umount /dev/sdb1` or `sudo umount /mnt/usbdisk/` interchangeably.

Optionally, you may remove your mounting directory. If it's something you will use multiple times, it doesn't hurt to leave the directory and reuse it later. It should be an empty directory if you unmounted the disk.

`sudo rmdir /mnt/usbdisk/`
