# SCP (Secure Copy)

Copies files between hosts on a network. It uses ssh for data transfer. This comes in handy transferring files from your desktop to your arcade pc or raspberry pi. Be careful as scp will overwrite files without warning.

---

## Terminal Syntax

Syntax | Description
:--- | :---
`$ scp [OPTION] [user@SRC_HOST:]file1 [user@DEST_HOST:]file2` | Copies files between hosts on a network.

---

## Usage Examples

Syntax | Description
:--- | :---
`$ sudo scp -r wiki/ pi@raspberrypi.local:~/` | Copy the wiki directory recursively to the user home directory on the raspberry pi.
