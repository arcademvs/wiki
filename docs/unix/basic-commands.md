# Basic Commands

Basic commands for the Unix family, including Linux and Mac OS X.

---

## Command-Chaining Operators

Syntax | Description
:--- | :---
AND - OR `&& - ||` | Combining and *AND* and *OR* operator acts like an *if-else* statement
AND `&&` | Execute proceeding command only if the preceding command succeeds with an exit status of *0*
Ampersand `&` | Run the preceding command in the background
Group `{}` | Group two or more commands, dependent on the preceding operand.
NOT `!` | Execute all except the condition provided
OR `||` | Execute proceeding command only if the preceding command fails with an exit status of *1*
PIPE `|` | Output from the preceding command is fed to the proceeding command
Precedence `()` | Execute commands with precedence such as in math
Semi-colon `;` | Run the preceding command sequentially

---

## File Management

#### cd

Change working directory.

Syntax | Description
:--- | :---
`$ cd ~/` | Change to home directory.
`$ cd ..` | Change working directory to one level back.

#### clear

Clear the terminal output.

Syntax | Description
:--- | :---
`$ clear` | Clear the terminal output.

#### find

Recursively finds files within path.

Syntax | Description
:--- | :---
`$ find ~/ -name basic-commands.md` | Finds  the file **basic-commands.md** within the users home directory.

#### locate

Find filenames quickly by using a database.

Syntax | Description
:--- | :---
`$ locate *.md -n 20` | List filenames ending with **.md**, and limit queries to 20.

#### ls

List files within working directory.

Syntax | Description
:--- | :---
`$ ls -1 *.md` | List files that ends with **.md**, and force output to be one entry per line.

#### man

Displays manual for the utility. Pressing **q** quits the process.

Syntax | Description
:--- | :---
`$ man cd` | Displays manual for the **ls** utility.

#### mkdir

Create directory if they do not already exist.

Syntax | Description
:--- | :---
`$ mkdir wiki` | Create directory named **wiki**.

#### mv

Move and or rename file/folder from source to destination.

Syntax | Description
:--- | :---
`$ mv basic-commands.md unix/basic-commands.md` | Move **basic-commands.md** to the **unix** directory, and name the file **commands.md**.

#### pwd

Return working directory name.

Syntax | Description
:--- | :---
`$ pwn` | Return working directory name.

#### rm

Removes directory entries. Be very careful with this command.

Syntax | Description
:--- | :---
`$ rm basic-commands.md` | Remove file named **basic-commands.md**.
`$ rm -rf  .DS_Store` | Removes all Mac Desktop Servies Store files, recursively.

#### rmdir

Removes directories.

Syntax | Description
:--- | :---
`$ rmdir docs` | Remove directory named **docs**.

#### touch

Create a file with default permissions.

Syntax | Description
:--- | :---
`$ touch basic-commands.md` | Create file **basic-commands.md**, if it does not exist. If it does, change modification and access times.

#### whereis

Locates source/binary and manuals sections for specified files in a list of *standard* Linux places.

Syntax | Description
:--- | :---
`$ whereis mame` | Where is the installed **mame** binary.

---

## Removing .DS_Store Files

Find and remove all matches, recursively. This is incredibly useful for removing Desktop Services Store files (Mac OSX). Be sure to change into your target directory first!

```
$ cd wiki
$ find . -name '.DS_Store' -type f -delete
```

---

## Power Management

#### halt

Stop the system.

Syntax | Description
:--- | :---
`$ sudo halt` | Stop the system.

#### reboot

Restart the system.

Syntax | Description
:--- | :---
`$ sudo reboot` | Restart the system.
