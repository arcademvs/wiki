# Install CRT-Emudriver for AMD/ATI Radeon HD5450+

Installing CRT-Emudriver is the same process for all AMD/ATI Radeon HD5450 cards and newer. This guide uses Windows 10. It should be very similar for Windows 7+.

---

## Confirm Default Video Driver

Default video driver is used with a fresh install of Windows. Standard SVGA resolution (800x600) will be assigned. We can confirm this:

![Display Adapter Properties](crtemudriver-install-5450/display-adapter-properties.jpg)

![Monitor Type: Generic](crtemudriver-install-5450/monitor-type-generic.jpg)

1. Connect SVGA/Multisync monitor to your video card and boot Windows. Do not connect your target monitor yet.
2. Select the Start button, then select Settings > System > Display > Display adapter properties
3. Monitor > Monitor Type > Generic Non-Pnp Monitor (default driver)

---

The rest of guide is pending me fixing the flyback transformer on my monitor. Please consider the donate button so I can continue guides like this.
