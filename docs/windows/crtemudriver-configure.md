# Configure CRT-Emudriver

## Adding other resolutions

Some steam games might require a resolution other than the default 640 x 480 interlaced. In this example, we will add 640 x 360 interlaced and 320 x 240 progressive.

#### Monitor Preset

We want to support lower interlaced lines. Edit ```monitor.ini```. The format is the following:

```
monitor "Name", "Description", "Format"
        crt_range 0-9   HfreqMin-HfreqMax, VfreqMin-VfreqMax, HFrontPorch, HSyncPulse, HBackPorch, VfrontPorch, VSyncPulse, VBackPorch, HSyncPol, VSyncPol, ProgressiveLinesMin, ProgressiveLinesMax, InterlacedLinesMin, InterlacedLinesMax
```

Find your current preset. Change the *InterlacedLinesMin* value to *360*. You may create a new preset based on your current one, in case you want to easily revert to the original. Below is an example:

```
monitor "k7000 custom", "Wells Gardner K7000 custom", "4:3"
        crt_range0 15625-15800, 49.50-63.00, 2.000, 4.700, 8.000, 0.064, 0.160, 1.056, 0, 0, 192, 288, 360, 576
```

#### User Modes

Edit ```user_modes - super.ini```. Add the following line:

```
 640 x 360 @ 60.000000 desktop
 320 x 240 @ 60.000000 desktop
```

Open VMMaker:

![VMMaker](crtemudriver-configure/vmmaker.jpg)

Edit settings > Monitor settings > Monitor presets > Type > Select your custom preset.

![VMMaker Monitor Settings Custom](crtemudriver-configure/vmmaker-monitor-settings-custom.jpg)

Generate modes:

![VMMaker Generate](crtemudriver-configure/vmmaker-generate.jpg)

Notice that we generated 25 modes in this example. Highlighted in photo is our new mode line.

Install modes:

![VMMaker Install](crtemudriver-configure/vmmaker-install.jpg)

Notice that we installed the same amount of modes as we generated. This is good. We can now play PC games with the resolutions we added.

![Arcade OSD](crtemudriver-configure/arcadeosd.jpg)

You can test and confirm the modes by running Arcade OSD.

---

## AMD External Events Utility

Symptom is screen doubling in attractmode.

> I believe this issue happens with software that sets the fullscreen resolution by a width & height pair of values, omitting the refresh, instead of using the correct width x height @ refresh unambiguous formula. If you allow Windows freedom to pick the refresh, it'll go to 31 kHz most of the times. Stopping AMD's service as described above masks this problem by not making those refresh rates available to Windows, but the underlying issue is still there in the weak mode setting logic inside the frontend/emulator. [ref](http://forum.arcadecontrols.com/index.php/topic,159832.msg1681464.html#msg1681464)

#### Stop AMD External Events Utility

1. Run (Win + R) > `services.msc`
2. In the service list, you should find one called "AMD External Events Utility" (if the service is not present in the list, then you probably don't have a correct Radeon drivers/CRT Emudriver installation).
3. Right-click on it and select "Properties"
4. On the panel: Click on "stop" and select "Disabled" as "Startup type".

#### Hide Modes

1. Select the Start button, then select Settings > System > Display.
2. In the **Settings** window, click **Display adapter properties**.
3. In the driver window, click the **Monitor** tab and check **Hide modes that this monitor cannot display**.
3. Click **Apply**.
4. Click **Close**.
