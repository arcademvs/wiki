# Multi-Video-System with Windows 10

Windows 10 is a great choice to run on a PC for a home arcade machine. The UI can easily be hidden. There is great support for games and emulators outside of MAME. There is no need to be familiar with CLI to set it up.

Requirements: Windows 10 v1703 (Creators Update) Enterprise or newer

---

## Installing Windows

Install Windows using a single account (admin) without a password.

---

## Configure Automatic Updates

MS Docs Source: [Docs/Windows/Deployment/Update Windows 10/Manage additional Windows Update settings](https://docs.microsoft.com/en-us/windows/deployment/update/waas-wu-settings)

#### Group Policy

*Must have pro or enterprise edition. Home does not support Group Policy.*

![Configure Automatic Updates 1](windows-10-setup/ConfigureAutomaticUpdates_00.jpg)

![Configure Automatic Updates 2](windows-10-setup/ConfigureAutomaticUpdates_01.jpg)

1. Run (Win + R) > ```gpedit.msc```
2. Under ```Computer Configuration\Administrative Templates\Windows Components\Windows update\Configure Automatic Updates```, you must select **Disabled**.

If your monitor resolution is a low resolution (crt), you may not be able to click the **Apply** button.

* Method 1:
    * Right click the task bar and uncheck **Lock the taskbar**.
    * Drag the task bar to either side of the screen.
* Method 2:
    * Click **Previous Setting** or **Next Setting**

#### Registry

![NoAutoUpdate](windows-10-setup/NoAutoUpdate.jpg)

1. Run (Win + R) > ```regedit.exe```
2. Open the following registry key: ```HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU```
3. Add the following registry values to configure Automatic Update:
    * Name: ```NoAutoUpdate```
    * Type: REG_DWORD
    * Value: **1** (Automatic Updates is disabled.)

---

## Automatic Logon

MS Docs Source: [How to turn on automatic logon in Windows](https://support.microsoft.com/en-us/help/324737/how-to-turn-on-automatic-logon-in-windows)

![Automatic Logon](windows-10-setup/AutomaticLogon.jpg)

1. Run (Win + R) > ```regedit.exe```
2. Open the following registry key: ```HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon```
3. Double-click the **DefaultUserName** entry, type your user name, and then click **OK**.
4. Double-click the **DefaultPassword** entry, type your password, and then click OK. *If the DefaultPassword value does not exist, it must be added.* To add the value, follow these steps:
    1. On the **Edit** menu, click **New**, and then point to **String Value**.
    2. Type ```DefaultPassword```, and then press Enter.
    3. Double-click **DefaultPassword**.
    4. In the **Edit String** dialog, type your password and then click OK.
5. On the **Edit** menu, click **New**, and then point to **String Value**.
6. Type ```AutoAdminLogon```, and then press **Enter**.
7. Double-click **AutoAdminLogon**.
8. In the **Edit String** dialog box, type ```1``` and then click **OK**.
9. Exit Registry Editor.
10. Restart your computer. You can now log on automatically.

---

## Annoyances

These items are annoying during gameplay.

#### Sticky Keys

MS Docs Source: [Make your mouse, keyboard, and other input devices easier to use](https://support.microsoft.com/en-us/help/27936/windows-10-make-input-devices-easier-to-use)

![Sticky Keys](windows-10-setup/StickyKeys.jpg)

"Sticky Keys let you press commands that use multiple keys (such as Ctrl + S) one key at a time." To activate this feature, you press the left shift key five times. This will interrupt your arcade gaming.

1. Select the Start button, then select Settings > Ease of Access > Keyboard
2. Turn **Sticky Keys** Off.

#### Game Bar Tips

![Game Bar Tips](windows-10-setup/GameBarTips.jpg)

Game Bar Tips is an overlay/alert that has to do with XBOX integration. This will interrupt your arcade gaming.

1. Run (Win + R) > ```regedit.exe```
2. Open the following registry key: ```HKEY_CURRENT_USER\Software\Microsoft\GameBar```
3. Double-click **ShowStartupPanel**.
4. In the **Edit String** dialog box, type ```0``` and then click **OK**.
5. Exit Registry Editor.
6. Restart your computer. Game Bar Tips will now be disabled.

#### Busy Cursor

![Busy Cursor](windows-10-setup/BusyCursor.jpg)

When MAME is loading, the busy (Blue Toilet Bowl) cursor is shown. This is distracting to the arcade experience.

[Download Hidden Cursor](windows-10-setup/hide.cur)

1. Run (Win + R) > ```control panel``` > Mouse.
2. In the **Mouse Properties** window, click the **Pointers** tab and then double click **Busy**.
3. In the **Browse** dialog box, select **hide.cur** and then click **Open**.
4. Click **Apply** and then **OK**.

---

## Unbranded Boot

MS Docs Source: [Unbranded Boot](https://docs.microsoft.com/en-us/windows-hardware/customize/enterprise/unbranded-boot)

Requirements: Windows 10 Enterprise, Windows 10 Professional, or Windows 10 Education.

![Unbranded Boot](windows-10-setup/UnbrandedBoot.jpg)

#### Turn on Unbranded Boot

![Device Lockdown](windows-10-setup/DeviceLockdown.jpg)

1. Run (Win + R) > Control Panel > Programs > Programs and Features > Turn Windows features on or off
2. In the Windows Features window, expand the **Device Lockdown** node, and check or clear the checkbox for **Unbranded Boot**.
3. Click **OK**.
4. Click **Close**.

#### Configure Unbranded Boot settings at runtime using BCDEdit

![Boot UX](windows-10-setup/BootUX.jpg)

To suppress all Windows UI elements (logo, status indicator, and status message) during startup:

1. Select the Start button, then select **Windows System**. Right click **Command Prompt** and select **More**, and then **Run as administrator**.
2. Type: ```bcdedit.exe -set {globalsettings} bootuxdisabled on```

---------

## Hide Auto Login UI

#### Turn on Custom Login

![Device Lockdown](windows-10-setup/DeviceLockdown.jpg)

1. Run (Win + R) > Control Panel > Programs > Programs and Features > Turn Windows features on or off
2. In the Windows Features window, expand the **Device Lockdown** node, and check or clear the checkbox for **Custom Logon**.
3. Click **OK**.
4. Click **Close**.

#### Configure Unbranded Boot Settings

MS Docs Source: [HideAutoLogonUI](https://docs.microsoft.com/en-us/windows-hardware/customize/desktop/unattend/microsoft-windows-embedded-embeddedlogon-hideautologonui)

1. Run (Win + R) > ```regedit.exe```
2. Open the following registry key: ```Computer\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Embedded\EmbeddedLogon```
3. Double-click **HideAutoLogonUI**.
4. In the **Edit String** dialog box, type ```1``` and then click **OK**.
5. Double-click **HideFirstLogonAnimation**.
6. In the **Edit String** dialog box, type ```1``` and then click **OK**.
7. Exit Registry Editor.

---

## Custom Shell

## Turn on Shell Launcher

MS Docs Source: [Kiosk shelllauncher](https://docs.microsoft.com/en-us/windows/configuration/kiosk-shelllauncher)

![Device Lockdown](windows-10-setup/DeviceLockdown.jpg)

1. Run (Win + R) > Control Panel > Programs > Programs and Features > Turn Windows features on or off
2. In the Windows Features window, expand the **Device Lockdown** node, and check or clear the checkbox for **Shell Launcher**.
3. Click **OK**.
4. Click **Close**.

## Configure Shell Settings

![Shell](windows-10-setup/Shell.jpg)

1. Run (Win + R) > ```regedit.exe```
2. Open the following registry key: ```Computer\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon```
3. Double-click **Shell**.
4. In the **Edit String** dialog box, type the path to your frontend (```C:\Arcade\attract\attract.exe --config C:\Arcade\attract\```) and then click **OK**.
7. Exit Registry Editor.

To launch explorer once shelled to another program:

1. Ctrl + Alt + Del > Task Manager > File > Run new task
2. Type **explorer** in the **Open:** dialog box, and click **OK**.

---

## Shutdown

#### Attractmode

1. Open ```attract.exe```.
2. Press **TAB** and select **Configure/Miscellaneous**.
3. Change **Exit Command** value to ```shutdown -s -t 0```.
