# Disk Image Management

A disk image is a copy of the entire contents of a storage device. Disk images are commonly used to create operating system installers or clone a disk.

---

## Etcher

![Etcher](etcher.jpg)

[Etcher](https://www.balena.io/etcher/) is a simple, multi-platform application to write images. Its free and you can download pre-made installers for Mac, Linux and Windows directly from their [homepage](https://www.balena.io/etcher/). They also include a guide on their [github repository](https://github.com/balena-io/etcher) readme for installing via the following:

Feature | Compatibility
:---: | :---:
Compatibility | Linux x64/x86 (AppImage)<br>Mac OS X x64<br>Windows x86/x64 (Installer/Portable)
Compatibility | Debian and Ubuntu based Package Repository (GNU/Linux x86/x64)<br>Redhat (RHEL) and Fedora based Package Repository (GNU/Linux x86/x64)<br>Solus (GNU/Linux x64)<br>Brew Cask (macOS)<br>Chocolatey (Windows)
Cost | Paid (trial/standard/pro)
Read | &#10004;
Write | &#10004;
Shrink/Expand | &#10004;

---

## dd (Disk Duplication)

dd is a CLI utility which reads and writes raw data. It comes with Linux and Mac OS X. **You must be careful with this utility as there is no warnings should you make a mistake.**

Feature | Compatibility
:---: | :---:
Compatibility | Linux<br>Mac OS X
Cost | Pre-Installed
Read | &#10004;
Write | &#10004;
Shrink/Expand | &#10008;

#### Usage

![diskutil](diskutil.jpg)

1. Display statistics about the filesystem.
    * Linux: ```$ lsblk```
    * Mac OS X: ```$ diskutil list```
2. Use *dd* to read or output a disk/image.

Command | Description
:--- | :---
```$ dd if=/dev/disk1 of=/dev/disk2``` | Read *disk1*, output *disk2*.
```$ dd if=~/disk.img of=/dev/disk1``` | Read *~/disk.img*, output *disk1*.
```$ dd if=/dev/disk1 of=~/disk.img``` | Read *disk1*, output *~/disk.img*.

---

## GParted

Feature | Compatibility
:---: | :---:
Compatibility | Live i686/amd64<br>Debian<br>Fedora<br>Mageia<br>OpenSUSE<br>Ubuntu
Cost | Free
Read | &#10008;
Write | &#10008;
Shrink/Expand | &#10004;

*guide soon to come*

---

## SD Clone

![SD Clone](sd-clone.jpg)

[SD Clone](https://twocanoes.com/products/mac/sd-clone/) works on all kinds of disks, not just SD cards.

Feature | Compatibility
:---: | :---:
Compatibility | Mac OS X 10.12+
Cost | Paid (trial/standard/pro)
Read | &#10004;
Write | &#10004;
Shrink/Expand | &#10004;

---

## Win32 Disk Imager

[Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/)

Feature | Compatibility
:---: | :---:
Compatibility | Windows 7/8.1/10<br>Windows Server 2008/2012/2016<br>Windows XP/Vista (v0.9)
Cost | Free
Read | &#10004;
Write | &#10004;
Shrink/Expand | &#10008;
