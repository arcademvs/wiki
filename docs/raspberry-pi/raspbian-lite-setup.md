# Raspbian Lite

Using Raspbian Lite for the raspberry pi is a great choice. It's lightweight and gives you control over what is installed and how things are configured. This guide uses Raspbian Stetch Lite. The steps in this guide may or may not work as intended.

---

## Login

You will need these default login credentials through out the guide.

* Username: Pi
* Password: raspberry

---

## Write Image To Disk

1. Download [Raspbian Lite](https://www.raspberrypi.org/downloads/raspbian/).
2. Follow steps outlined in the [Disk Images](/disk-images) section of this wiki to write the image to an SD card.

---

## Configure Settings

After writing the image to disk, these configuration settings can be edited from your pc. Official documentation [here](https://www.raspberrypi.org/documentation/configuration/). Commands for editing are for Mac/Linux. Windows users can use the *notepad* CLI utility instead of *nano*.

#### Boot Config

We are going to set it up for composite video 240 (CRT). See official video configuration documentation [here](https://www.raspberrypi.org/documentation/configuration/config-txt/video.md) if you are using HDMI. In the case of HDMI use, lines starting with *SDTV* will not be necessary. [gpu_mem](https://www.raspberrypi.org/documentation/configuration/config-txt/memory.md) is important to change before compiling.

```
$ sudo nano /boot/config.txt
```

```
sdtv_mode=16 # 0=480i, 16=240p
sdtv_aspect=1 # 4:3 aspect ratio
disable_splash=1 # Remove rainbow screen on boot
gpu_mem=320 # 320=normal, 16=compiling
```

#### Command Line Config

```
$ sudo nano /boot/cmdline.txt
```

Add the following options to the end of the line.

* Turn off raspberries logo: ```logo.nologo```
* Turn off blinking cursor: ```vt.global_cursor_default=0```

#### Networking

Create the following files to connect to a WiFi network and use SCP/SSH. Connecting to a network means that you can use SCP to copy files and SSH for configuration.

###### WiFi

```
$ sudo touch /rootfs/etc/wpa_supplicant/wpa_supplicant.conf
$ nano /rootfs/etc/wpa_supplicant/wpa_supplicant.conf
```

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=US

network={
	ssid="NAME"
	psk="PASSWORD"
	key_mgmt=WPA-PSK
}

```

###### SSH

```
$ sudo touch /boot/ssh
```

#### First boot

It's time to boot your raspberry pi for the first time. Insert the SD card into the raspberry pi, connect your composite cables to your CRT, connect a USB keyboard, and turn it on. Note that Raspberry pi 3.5mm composite breakout cables are slightly different than standard camcorder 3.5mm A/V breakout cables. The composite signal will be output on the red RCA (left audio), and left audio will be output on the yellow RCA (composite video).

You will be prompted with a terminal. Login with the following credentials:

* Username: Pi
* Password: raspberry

Run the Raspberry Pi Config GUI: ```$ sudo raspi-config```

Select the following options to expand the filesystem to the full size of the SD card:

1. Advanced Options
    * Expand Filestytem
2. Boot Options
    * Desktop / CLI > Console Autologin
3. Localisation Options
    * Overclock > High *Option for 3B+, must have at heatsinks installed*

#### Connecting via SSH

To connect you must first find out the IP address of the Raspberry Pi by running ```$ ifconfig```. Run either of the following commands from the remote terminal.

* ```$ ssh pi@<IPADDRESS>```
* ```$ ssh pi@<HOSTNAME>.local```
* ```$ ssh pi@raspberrypi.local```

To exit a remote connection, run the ```exit``` command.

#### Overscan

You can download and install this script to make setting overscan easy, without having to guess and reboot each time.

```
$ cd ~/
$ wget https://github.com/ukscone/set_overscan/archive/master.zip
$ unzip master.zip && rm master.zip
$ cd set_overscan-master
$ sudo make
$ sudo ./set_overscan.sh
```

Or edit manually:

```
$ sudo nano /boot/config.txt
```

```
overscan_left=24
overscan_right=24
overscan_top=24
overscan_bottom=24
```

#### Audio

Change audio to these settings

Set volume to 100%:

```
$ amixer sset PCM,0 100%
$ speaker-test -c2 -t wav
```

#### RetroFlag

If using a RetroFlag case, this will set up the required script:

```
$ wget -O - "https://raw.githubusercontent.com/RetroFlag/retroflag-picase/master/install.sh" | sudo bash```
$ sudo nano /opt/RetroFlag/SafeShutdown.py
```

Edit lines 19 and 23 to exclude references to emulation and sleep:

```
# line 19
# os.system("sudo killall emulationstation && sleep 5s && sudo shutdown -h now")
os.system("sudo shutdown -h now")

# line 23
# os.system("sudo killall emulationstation && sleep 5s && sudo reboot")
os.system("sudo reboot")
```

---

## SDL

Compiling SDL is required to compile [MAME](#mame). Performance is not great on the Raspberry Pi. This is for testing purposes only. It is recommended to install [AdvancedMAME](#advancemame) for game play.

> SDL2 is now installed by default but it is compiled for desktop users with support for X and OpenGL, neither of which we can take advantage of in a framebuffer and it will just confuse MAME if you try to compile using it.

> Any program that uses SDL2 to draw to the framebuffer should now be doing it in hardware on the raspberry pis videocore4 using opengles2!

#### Uninstall SDL

```libsdl2-dev``` is not installed on raspbian stretch lite. If using the desktop version, you must uninstall it.

```
$ sudo apt-get remove -y --force-yes libsdl2-dev
$ sudo apt-get autoremove -y
```

#### Add Dependencies

Add packages for build environment.

```
$ sudo apt-get install libfontconfig-dev qt5-default automake mercurial libtool libfreeimage-dev libopenal-dev libpango1.0-dev libsndfile-dev libudev-dev libtiff5-dev libwebp-dev libasound2-dev libaudio-dev libxrandr-dev libxcursor-dev libxi-dev libxinerama-dev libxss-dev libesd0-dev freeglut3-dev libmodplug-dev libsmpeg-dev libjpeg-dev
```

#### Install SDL

Get the latest libsdl and compile:

```
$ mkdir ~/development
$ cd ~/development
$ hg clone http://hg.libsdl.org/SDL
$ cd SDL
$ ./autogen.sh
$ ./configure --disable-pulseaudio --disable-esd --disable-video-mir --disable-video-wayland --disable-video-opengl --host=arm-raspberry-linux-gnueabihf```
$ make
$ sudo make install
```

#### SDL Libraries

Download and uncompress all the utility libraries.

```
$ cd ~/development
$ wget http://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.2.tar.gz
$ wget http://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-2.0.2.tar.gz
$ wget http://www.libsdl.org/projects/SDL_net/release/SDL2_net-2.0.1.tar.gz
$ wget http://www.libsdl.org/projects/SDL_ttf/release/SDL2_ttf-2.0.14.tar.gz
$ tar zxvf SDL2_image-2.0.2.tar.gz
$ tar zxvf SDL2_mixer-2.0.2.tar.gz
$ tar zxvf SDL2_net-2.0.1.tar.gz
$ tar zxvf SDL2_ttf-2.0.14.tar.gz
```

Build the Image file loading library.

```
$ cd $ ~/development/SDL2_image-2.0.2
$ ./autogen.sh
$ ./configure
$ make
$ sudo make install
```

Build the Audio mixer library.

```
$ cd ~/development/SDL2_mixer-2.0.2
$ ./autogen.sh
$ ./configure
$ make
$ sudo make install
```

Build the Networking library.

```
$ cd ~/development/SDL2_net-2.0.1
$ ./autogen.sh
$ ./configure
$ make
$ sudo make install
```

Build the Truetype font library.

```
$ cd SDL2_ttf-2.0.14
$ ./autogen.sh
$ ./configure
$ make
$ sudo make install
```

---

## MAME

Performance is not great on the Raspberry Pi. This is for testing purposes only. It is recommended to install [AdvancedMAME](#advancemame) for game play.

#### Compile

###### Change Swap Size

1. ```$ sudo nano /etc/dphys-swapfile```
2. Change ```CONF_SWAPSIZE=100``` to ```CONF_SWAPSIZE=2048```.
3. ```$ reboot```.

###### Download and Compile MAME

You can substitute MAME version with a different one.

```
$ mkdir ~/development
$ cd ~/development
$ wget -O - https://github.com/mamedev/mame/releases/download/mame0213/mame0213s.zip > mame0213s.zip && unzip mame0213s.zip - d ~/
$ cd ~/mame
$ make -j5
```

###### Change Swap Size

1. ```$ sudo nano /etc/dphys-swapfile```
2. Change ```CONF_SWAPSIZE=2048``` back to ```CONF_SWAPSIZE=100```.
3. ```$ reboot```.

#### Run

To generate config: ```$ ~/mame/./mame -cc```

To run a game: ```$ ~/mame/./mame romname```

---

## AdvancedMAME

```
$ mkdir ~/development
$ cd ~/development
$ wget https://github.com/amadvance/advancemame/releases/download/v3.9/advancemame_3.9-1_armhf.deb
$ sudo apt install ~/development/advancemame_3.9-1_armhf.deb
```

You can now run the command ```$ advmame``` to create configuration files, placed in the ```~/.advance``` directory. Binary is located in ```/usr/local/bin```.

To run a game: ```$ advmame romname```

---

## Attractmode

#### SFML

```
$ mkdir ~/development
$ cd ~/development
$ wget -O - https://github.com/mickelson/sfml-pi/archive/master.zip > sfml-pi-master.zip && unzip sfml-pi-master.zip
$ cd sfml-pi-master/cmake
$ cmake .. -DSFML_RPI=1 -DEGL_INCLUDE_DIR=/opt/vc/include -DEGL_LIBRARY=/opt/vc/lib/libbrcmEGL.so -DGLES_INCLUDE_DIR=/opt/vc/include -DGLES_LIBRARY=/opt/vc/lib/libbrcmGLESv2.so
$ sudo ldconfig
```

#### FFmpeg

```
$ cd ~/development
$ wget https://ffmpeg.org/releases/ffmpeg-4.1.tar.bz2
$ tar -xf ffmpeg-4.1.tar.bz2
$ cd ffmpeg-4.1
$ ./configure --enable-mmal --disable-debug --enable-shared
$ make
`$ sudo make install
$ sudo ldconfig
```

#### AttractMode

```
$ cd ~/development
$ wget -O - https://github.com/mickelson/attract/archive/master.zip > attract-master.zip && unzip attract-master.zip
$ cd attract-master
$ make USE_GLES=1
$ sudo make install USE_GLES=1
```

To run attractmode, run ```$ attract```. Binary is located in ```/usr/local/bin```. Data folder is ```/home/pi/.attract```.

---

## Cleanup

Optionally, you can remove your development folder if not compiling or installing anything else.

```
$ rm -rf ~/development
```

---

## Load on Login

As suggested under config settings part of the guide, be sure you used ```$ sudo raspi-config``` to set the following:

* Boot Options
    * Desktop / CLI > Console Autologin

```.bash_profile``` of the user is executed to configure your shell before the initial command prompt. Our script will launch attract if the current user (pi) logs in and it is not an ssh connection.

```
$ touch ~/.bash_profile
$ chmod -x ~/.bash_profile
$ nano ~/.bash_profile
```

You may replace the path to the attract binary with advmame (single rom instance?), or anything else you wish to have automatically loaded.

```
#!/bin/sh
[ -n "${SSH_CONNECTION}" ] || /usr/local/bin/attract
```

---

## Bluetooth Controllers

I am using an 8bitdo m30 controller. However, this should work similar for most other controllers.

1. ```$ bluetoothctl```
2. Put the remote into d-input mode by holding start + B until LED 1 blinks.
3. Put the remote into pair-able mode by holding the small pair button on the front of the remote for 2 seconds. LEDs will sequence.
4. At the ```[bluetooth]#``` prompt, enter the following commands:

```
[bluetooth]# discoverable on
[bluetooth]# pairable on
[bluetooth]# agent on
[bluetooth]# default-agent
[bluetooth]# scan on
Discovery started
[CHG] Controller B8:27:EB:7B:89:3C Discovering: yes
[NEW] Device E4:17:D8:87:09:7D 8BitDo M30 gamepad
[bluetooth]# pair E4:17:D8:87:09:7D
[bluetooth]# trust E4:17:D8:87:09:7D
[bluetooth]# connect E4:17:D8:87:09:7D
[bluetooth]# scan off
[bluetooth]# paired-devices
Device E4:17:D8:87:09:7D 8BitDo M30 gamepad
[bluetooth]#quit
```

Install joystick tools for testing:

```
sudo apt-get install joystick
```

See if your controllers are detected:

```
cat /proc/bus/input/devices
```

Once your controllers are detected, a special event file should have been created for each one:

```
ls /dev/input/js*
```

Test joysticks. In my case, I have two of them:

```
$ jstest /dev/input/js0
$ jstest /dev/input/js1
```
